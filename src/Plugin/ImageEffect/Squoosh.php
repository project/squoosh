<?php

declare(strict_types=1);

namespace Drupal\squoosh\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Transforms an image resource.
 *
 * @ImageEffect(
 *   id = "squoosh",
 *   label = @Translation("Squoosh"),
 *   description = @Translation("Perform Squoosh transformations on an image.")
 * )
 */
final class Squoosh extends ConfigurableImageEffectBase {

  private const ENCODING_EXTENSION_MAP = [
    '' => '',
    'avif' => 'avif',
    'browserJPEG' => 'jpg',
    'browserPNG' => 'png',
    'jxl' => 'jpg',
    'mozJPEG' => 'jpg',
    'oxiPNG' => 'png',
    'webP' => 'webp',
    'wp2' => 'wp2',
  ];

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeExtension($extension): string {
    return self::ENCODING_EXTENSION_MAP[$this->configuration['encoding']] ?: $extension;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(): array {
    $summary = [
      '#markup' => $this->encodingOptions()[$this->configuration['encoding']],
    ];
    $summary += parent::getSummary();

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'encoding' => 'mozJPEG',
      'quality' => 75,
      'resize' => FALSE,
      'width' => NULL,
      'height' => NULL,
      'reduce_palette' => FALSE,
      'reduce_colors' => 256,
      'dithering' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['encoding'] = [
      '#type' => 'select',
      '#title' => $this->t('Convert to'),
      '#empty_option' => $this->t('Original Image'),
      '#default_value' => $this->configuration['encoding'],
      '#options' => $this->encodingOptions(),
    ];
    $form['quality'] = [
      '#type' => 'number',
      '#title' => $this->t('Quality'),
      '#default_value' => $this->configuration['quality'],
      '#min' => 0,
      '#max' => 100,
      '#step' => 0.1,
      '#states' => [
        'invisible' => [
          ':input[name="data[encoding]"]' => ['value' => ''],
        ],
      ],
    ];
    $form['resize'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Resize'),
      '#default_value' => $this->configuration['resize'],
      '#states' => [
        'invisible' => [
          ':input[name="data[encoding]"]' => ['value' => ''],
        ],
      ],
    ];
    $form['maintain_aspect_ratio'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Maintain Aspect Ratio'),
      '#description' => $this->t('If only height or width is specified, aspect ratio will be maintained.'),
      '#attributes' => [
        'disabled' => 'disabled',
      ],
      '#states' => [
        'invisible' => [
          ':input[name="data[encoding]"]' => ['value' => ''],
        ],
        'visible' => [
          ':input[name="data[resize]"]' => ['checked' => TRUE],
        ],
        'checked' => [
          [
            'input[name="data[width]"]' => ['value' => 0],
            'input[name="data[height]"]' => ['filled' => TRUE],
          ],
          'or',
          [
            'input[name="data[width]"]' => ['filled' => FALSE],
            'input[name="data[height]"]' => ['filled' => TRUE],
          ],
          'or',
          [
            'input[name="data[height]"]' => ['value' => 0],
            'input[name="data[width]"]' => ['filled' => TRUE],
          ],
          'or',
          [
            'input[name="data[height]"]' => ['filled' => FALSE],
            'input[name="data[width]"]' => ['filled' => TRUE],
          ],
        ],
      ],
    ];
    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->configuration['width'],
      '#min' => 0,
      '#states' => [
        'invisible' => [
          ':input[name="data[encoding]"]' => ['value' => ''],
        ],
        'visible' => [
          ':input[name="data[resize]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->configuration['height'],
      '#min' => 0,
      '#states' => [
        'invisible' => [
          ':input[name="data[encoding]"]' => ['value' => ''],
        ],
        'visible' => [
          ':input[name="data[resize]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['reduce_palette'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reduce palette'),
      '#default_value' => $this->configuration['reduce_palette'],
      '#states' => [
        'invisible' => [
          ':input[name="data[encoding]"]' => ['value' => ''],
        ],
      ],
    ];
    $form['reduce_colors'] = [
      '#type' => 'number',
      '#title' => $this->t('Colors'),
      '#default_value' => $this->configuration['reduce_colors'],
      '#min' => 2,
      '#max' => 256,
      '#states' => [
        'invisible' => [
          ':input[name="data[encoding]"]' => ['value' => ''],
        ],
        'visible' => [
          ':input[name="data[reduce_palette]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['dithering'] = [
      '#type' => 'number',
      '#title' => $this->t('Dithering'),
      '#default_value' => $this->configuration['dithering'],
      '#min' => 0,
      '#max' => 1,
      '#step' => 0.01,
      '#states' => [
        'invisible' => [
          ':input[name="data[encoding]"]' => ['value' => ''],
        ],
        'visible' => [
          ':input[name="data[reduce_palette]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return $form;
  }

  private function encodingOptions(): array {
    return [
      'avif' => $this->t('AVIF'),
      'browserJPEG' => $this->t('Browser JPEG'),
      'browserPNG' => $this->t('Browser PNG'),
      'jxl' => $this->t('JPEG XL (beta)'),
      'mozJPEG' => $this->t('MozJPEG'),
      'oxiPNG' => $this->t('OxiPNG'),
      'webP' => $this->t('WebP'),
      'wp2' => $this->t('WebP v2 (unstable)'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    foreach (\array_keys(self::defaultConfiguration()) as $key) {
      $this->configuration[$key] = $form_state->getValue($key);
    }
  }

}
