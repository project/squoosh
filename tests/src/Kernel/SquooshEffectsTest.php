<?php

declare(strict_types=1);

namespace Drupal\Tests\squoosh\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Form\ImageEffectEditForm;
use Drupal\image\ImageEffectInterface;
use Drupal\image\ImageStyleInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test squoosh image effect.
 *
 * @group squoosh
 */
final class SquooshEffectsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'squoosh',
    'system',
    'image',
  ];

  /**
   * Tests that validation errors are passed from the plugin to the parent form.
   */
  public function testEffectFormValidationErrors(): void {
    $form_builder = $this->container->get('form_builder');

    $image_style = ImageStyle::create(['name' => $this->randomMachineName()]);
    \assert($image_style instanceof ImageStyleInterface);
    $effect_id = $image_style->addImageEffect(['id' => 'squoosh']);
    $image_style->save();

    $form = new ImageEffectEditForm();
    $form_state = new FormState();
    $form_builder->submitForm($form, $form_state, $image_style, $effect_id);

    $effect = $image_style->getEffect($effect_id);
    \assert($effect instanceof ImageEffectInterface);
    self::assertEquals($effect->defaultConfiguration(), $effect->getConfiguration()['data']);
  }

}
