<?php

declare(strict_types=1);

namespace Drupal\Tests\squoosh\Functional;

use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Test squoosh functionality.
 *
 * @group squoosh
 */
final class SquooshTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'squoosh',
    'image',
  ];

  /**
   * Test configuration schema.
   */
  public function testSquooshConfiguration(): void {
    $image_style = ImageStyle::create(['name' => $this->randomMachineName()]);
    \assert($image_style instanceof ImageStyleInterface);
    $image_style->addImageEffect(['id' => 'squoosh']);
    $image_style->save();

    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer image styles',
    ]);
    $this->drupalLogin($admin_user);
    $this->drupalGet(Url::fromRoute('entity.image_style.edit_form', ['image_style' => $image_style->id()]));
    $this->assertSession()->responseContains('Squoosh');
  }

}
