# Squoosh

---------------

## About this Module

Squoosh provides image effect integration for
https://github.com/GoogleChromeLabs/squoosh.

### Installing the Squoosh Module

Note: Use of the module on a Composer managed site is also supported.

1. Copy/upload the module to the modules' directory of your Drupal installation.
2. Enable the module in 'Extend' (/admin/modules).
3. Configure Squoosh image effect on an image style.
   (/admin/config/media/image-styles).

Embed the Sqoosh JS library in any of several ways. The following method will
place the squoosh library at the expected location, `libraries/squoosh`.

#### composer.json
```json
{
  "require": {
    "google/squoosh": "dev-master"
  },
  "repositories": [
    {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    },
    {
      "type": "package",
      "package": {
        "name": "google/squoosh",
        "version": "dev-master",
        "type": "drupal-library",
        "dist": {
          "url": "https://github.com/GoogleChromeLabs/squoosh/archive/dev.zip",
          "type": "zip"
        }
      }
    }
  ]
}
```

After the code is download, then install it following its instructions.
Namely:
```shell
cd libraries/squoosh
npm install
npm run build
npm run dev
```
